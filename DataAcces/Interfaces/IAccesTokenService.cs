﻿using DataAcces.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcces.Interfaces
{
   public interface IAccesTokenService
    {
     Task<string> GenerateUserTokenAsync(User userProfile);
    }
}
