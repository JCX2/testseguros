﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcces.Interfaces
{
    public interface IEncryptHash
    {
        string EncriptarTexto(string texto);
        string DesencriptarTexto(string textoEncriptado);
    }
}
