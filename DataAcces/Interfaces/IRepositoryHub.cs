﻿using DataAcces.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcces.Interfaces
{
    public interface IRepositoryHub
    {
        IRepository<Customer> CustomerRepository { get; set; }

        IRepository<CustomerInsurance> CustomerInsuranceRepository { get; set; }

        IRepository<Insurance> InsuranceRepository { get; set; }

        IRepository<InsurancesType> InsurancesTypeRepository { get; set; }

        IRepository<User> UserRepository { get; set; }
    }
}
