﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DataAcces.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        Task<TEntity> CreateAsync(TEntity newRecord);

        Task<TEntity> GetFirstRowAsync(Expression<Func<TEntity, bool>> predicate);

        Task<TEntity> FindByIdAsync(Guid id);

        Task<bool> UpdateOrCreate(TEntity entity);

        Task<List<TEntity>> GetAllAsync();

        Guid GetKey(TEntity entity);

        Task<bool> DeleteAsync(Guid entity);

        Task AddEntity(TEntity entity);
    }
}
