﻿using DataAcces.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcces.Interfaces
{
    public interface IUser
    {
        /// <summary>
        /// Valida el usuario para ingresar a la aplicacion web
        /// </summary>
        /// <param name="username">Usuario</param>
        /// <param name="password">Contraseña</param>
        /// <returns>True si las credenciales son validas</returns>
        Task<bool> ValidateUserRequest(string username, string password);
        /// <summary>
        /// Obtiene un perfil de usuario autenticado
        /// </summary>
        /// <param name="username">Nombre de usuario</param>
        /// <returns>Perfil del usuario</returns>
        Task<User> GetUserProfile(string username);
    }
}
