﻿namespace DataAcces.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Insurancev1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CustomerInsurances",
                c => new
                    {
                        CustomerInsuranceId = c.Guid(nullable: false),
                        id_customer = c.Guid(nullable: false),
                        id_insurance = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.CustomerInsuranceId)
                .ForeignKey("dbo.Customers", t => t.id_customer, cascadeDelete: true)
                .ForeignKey("dbo.Insurances", t => t.id_insurance, cascadeDelete: true)
                .Index(t => t.id_customer)
                .Index(t => t.id_insurance);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        CustomerId = c.Guid(nullable: false),
                        name = c.String(nullable: false),
                        phone = c.String(),
                        email = c.String(),
                        identificationCode = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.CustomerId);
            
            CreateTable(
                "dbo.Insurances",
                c => new
                    {
                        InsuranceId = c.Guid(nullable: false),
                        name = c.String(nullable: false),
                        description = c.String(nullable: false),
                        intialDate = c.DateTime(nullable: false),
                        insuranceTime = c.Int(nullable: false),
                        price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        risk = c.String(nullable: false),
                        id_insurance_type = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.InsuranceId)
                .ForeignKey("dbo.InsurancesTypes", t => t.id_insurance_type, cascadeDelete: true)
                .Index(t => t.id_insurance_type);
            
            CreateTable(
                "dbo.InsurancesTypes",
                c => new
                    {
                        InsurancesTypeId = c.Guid(nullable: false),
                        name = c.String(),
                        percentage = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.InsurancesTypeId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        UserId = c.Guid(nullable: false),
                        name = c.String(),
                        password = c.String(),
                    })
                .PrimaryKey(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CustomerInsurances", "id_insurance", "dbo.Insurances");
            DropForeignKey("dbo.Insurances", "id_insurance_type", "dbo.InsurancesTypes");
            DropForeignKey("dbo.CustomerInsurances", "id_customer", "dbo.Customers");
            DropIndex("dbo.Insurances", new[] { "id_insurance_type" });
            DropIndex("dbo.CustomerInsurances", new[] { "id_insurance" });
            DropIndex("dbo.CustomerInsurances", new[] { "id_customer" });
            DropTable("dbo.Users");
            DropTable("dbo.InsurancesTypes");
            DropTable("dbo.Insurances");
            DropTable("dbo.Customers");
            DropTable("dbo.CustomerInsurances");
        }
    }
}
