﻿namespace DataAcces.Migrations
{
    using DataAcces.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DataAcces.Models.InsuranceContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DataAcces.Models.InsuranceContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
            if (!context.Users.Any()) 
                context.Users.AddOrUpdate(new User { name = "admin", password = "57rw4IvQUm3nWT7PWt+Xjg==" });
            if (!context.InsurancesTypes.Any())
            {
                context.InsurancesTypes.AddOrUpdate(new InsurancesType { name = "Terremoto", percentage = 20 });
                context.InsurancesTypes.AddOrUpdate(new InsurancesType { name = "Incendio", percentage = 30 });
                context.InsurancesTypes.AddOrUpdate(new InsurancesType { name = "Robo", percentage = 80 });
                context.InsurancesTypes.AddOrUpdate(new InsurancesType { name = "Pérdida", percentage = 60 });
            }


        }
    }
}
