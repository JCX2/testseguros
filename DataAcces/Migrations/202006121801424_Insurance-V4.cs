﻿namespace DataAcces.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InsuranceV4 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.CustomerInsurances", "id_customer", "dbo.Customers");
            DropForeignKey("dbo.CustomerInsurances", "id_insurance", "dbo.Insurances");
            DropForeignKey("dbo.Insurances", "id_insurance_type", "dbo.InsurancesTypes");
            DropPrimaryKey("dbo.CustomerInsurances");
            DropPrimaryKey("dbo.Customers");
            DropPrimaryKey("dbo.Insurances");
            DropPrimaryKey("dbo.InsurancesTypes");
            DropPrimaryKey("dbo.Users");
            AlterColumn("dbo.CustomerInsurances", "CustomerInsuranceId", c => c.Guid(nullable: false, identity: true ,defaultValueSql: "newid()"));
            AlterColumn("dbo.Customers", "CustomerId", c => c.Guid(nullable: false, identity: true,  defaultValueSql: "newid()"));
            AlterColumn("dbo.Insurances", "InsuranceId", c => c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"));
            AlterColumn("dbo.InsurancesTypes", "InsurancesTypeId", c => c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"));
            AlterColumn("dbo.Users", "UserId", c => c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"));
            AddPrimaryKey("dbo.CustomerInsurances", "CustomerInsuranceId");
            AddPrimaryKey("dbo.Customers", "CustomerId");
            AddPrimaryKey("dbo.Insurances", "InsuranceId");
            AddPrimaryKey("dbo.InsurancesTypes", "InsurancesTypeId");
            AddPrimaryKey("dbo.Users", "UserId");
            AddForeignKey("dbo.CustomerInsurances", "id_customer", "dbo.Customers", "CustomerId", cascadeDelete: true);
            AddForeignKey("dbo.CustomerInsurances", "id_insurance", "dbo.Insurances", "InsuranceId", cascadeDelete: true);
            AddForeignKey("dbo.Insurances", "id_insurance_type", "dbo.InsurancesTypes", "InsurancesTypeId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Insurances", "id_insurance_type", "dbo.InsurancesTypes");
            DropForeignKey("dbo.CustomerInsurances", "id_insurance", "dbo.Insurances");
            DropForeignKey("dbo.CustomerInsurances", "id_customer", "dbo.Customers");
            DropPrimaryKey("dbo.Users");
            DropPrimaryKey("dbo.InsurancesTypes");
            DropPrimaryKey("dbo.Insurances");
            DropPrimaryKey("dbo.Customers");
            DropPrimaryKey("dbo.CustomerInsurances");
            AlterColumn("dbo.Users", "UserId", c => c.Guid(nullable: false));
            AlterColumn("dbo.InsurancesTypes", "InsurancesTypeId", c => c.Guid(nullable: false));
            AlterColumn("dbo.Insurances", "InsuranceId", c => c.Guid(nullable: false));
            AlterColumn("dbo.Customers", "CustomerId", c => c.Guid(nullable: false));
            AlterColumn("dbo.CustomerInsurances", "CustomerInsuranceId", c => c.Guid(nullable: false));
            AddPrimaryKey("dbo.Users", "UserId");
            AddPrimaryKey("dbo.InsurancesTypes", "InsurancesTypeId");
            AddPrimaryKey("dbo.Insurances", "InsuranceId");
            AddPrimaryKey("dbo.Customers", "CustomerId");
            AddPrimaryKey("dbo.CustomerInsurances", "CustomerInsuranceId");
            AddForeignKey("dbo.Insurances", "id_insurance_type", "dbo.InsurancesTypes", "InsurancesTypeId", cascadeDelete: true);
            AddForeignKey("dbo.CustomerInsurances", "id_insurance", "dbo.Insurances", "InsuranceId", cascadeDelete: true);
            AddForeignKey("dbo.CustomerInsurances", "id_customer", "dbo.Customers", "CustomerId", cascadeDelete: true);
        }
    }
}
