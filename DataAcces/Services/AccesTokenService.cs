﻿using DataAcces.Interfaces;
using DataAcces.Models;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;


namespace DataAcces.Services
{
    public class AccesTokenService : IAccesTokenService
    {
        private string _securityKey;

        public AccesTokenService(string securityKey)
        {
            _securityKey = securityKey;
        }

        private string GenerateToken(Claim[] claims)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_securityKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                claims: claims,
                expires: DateTime.Now.AddDays(30),
                signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public async Task<string> GenerateUserTokenAsync(User userProfile)
        {
            var tokenStr = await Task.Run(() =>
            {
                var claims = new[]
                {
                    new Claim("RetailerId", userProfile.UserId.ToString()),
                    new Claim("Username", userProfile.name)
                };
                return GenerateToken(claims);
            });
            return tokenStr;
        }
    }
}
