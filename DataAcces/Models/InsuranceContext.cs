﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.Core;
using DataAcces.Properties;

namespace DataAcces.Models
{
    public class InsuranceContext : DbContext
    {
        public InsuranceContext() : base("Insurance")
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<InsuranceContext, DataAcces.Migrations.Configuration>());
           
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<CustomerInsurance> CustomerInsurances { get; set; }
        public DbSet<Insurance> Insurances { get; set; }
        public DbSet<InsurancesType> InsurancesTypes { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Insurance>().Property(x => x.price).HasColumnType(Resources.udtAmount);
            //modelBuilder.Entity<Insurance>().Property(x => x.insuranceTime).HasColumnType(Resources.udtInteger);
            //modelBuilder.Entity<InsurancesType>().Property(x => x.percentage).HasColumnType(Resources.udtPercentage);
        }
    }
}
