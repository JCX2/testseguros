﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcces.Models
{
    public class Insurance
    {
        public Insurance()
        {
            Customers = new HashSet<CustomerInsurance>();
        }

        [Required]
        public string name { get; set; }
        [Required]
        public string description { get; set; }
        [Required]
        public DateTime intialDate { get; set; }
        [Required]
        public int insuranceTime { get; set; }
        [Required]
        public decimal price { get; set; }
        [Required]
        public string risk { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid InsuranceId { get; set; }
        [Required]
        [ForeignKey("InsurancesType")]
        public Guid id_insurance_type { get; set; }

        public virtual ICollection<CustomerInsurance> Customers { get; set; }

        public virtual InsurancesType InsurancesType  { get;set;}
    }
}
