﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcces.Models
{
    public class CustomerInsurance
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid CustomerInsuranceId { get; set; }
        [ForeignKey("Customer")]
        public Guid id_customer { get; set; }
        [ForeignKey("Insurance")]
        public Guid id_insurance { get; set; }

        public virtual Insurance Insurance{ get; set; }
        public virtual Customer Customer { get; set; }
    }
}
