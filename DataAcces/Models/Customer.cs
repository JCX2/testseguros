﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcces.Models
{
   public class Customer
    {
        public Customer()
        {
            Insurances = new HashSet<CustomerInsurance>();
        }
        [Required]
        public string name { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        [Required]
        public string identificationCode { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid CustomerId { get; set; }

        public virtual ICollection<CustomerInsurance> Insurances { get; set; }
    }
}
