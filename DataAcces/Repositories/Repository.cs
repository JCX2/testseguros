﻿using DataAcces.Interfaces;
using DataAcces.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DataAcces.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected InsuranceContext _dbContext;
        private string _keyName;
        public Repository(InsuranceContext dbContext)
        {
            _dbContext = dbContext;
            ObjectContext objectContext = ((IObjectContextAdapter)dbContext).ObjectContext;
            ObjectSet<TEntity> set = objectContext.CreateObjectSet<TEntity>();
            IEnumerable<string> keyNames = set.EntitySet.ElementType
                                                        .KeyMembers
                                                        .Select(k => k.Name);
            _keyName = keyNames.FirstOrDefault();
        }

        public virtual Guid GetKey(TEntity entity)
        {
            return (Guid)entity.GetType().GetProperty(_keyName).GetValue(entity, null);
        }

        public async Task AddEntity(TEntity entity)
        {
            await Task.Run(() =>
           {
               _dbContext.Set<TEntity>().Add(entity);
               _dbContext.SaveChangesAsync();
           });
        }

        public TEntity Create(TEntity newRecord)
        {
            try
            {
                _dbContext.Set<TEntity>().Add(newRecord);
                _dbContext.SaveChanges();

                return newRecord;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public async Task<TEntity> CreateAsync(TEntity newRecord)
        {
            return await Task.Run(() =>
            {
                return Create(newRecord);
            });
        }

        public async Task<bool> DeleteAsync(Guid entity)
        {
            return await Task.Run(() =>
            {
                return Delete(entity);
            });
        }

        public async Task<TEntity> FindByIdAsync(Guid id)
        {
            return await _dbContext.Set<TEntity>().FindAsync(id);
        }

        public async Task<List<TEntity>> GetAllAsync()
        {
            return await _dbContext.Set<TEntity>().ToListAsync();
        }

        public Task<TEntity> GetFirstRowAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbContext.Set<TEntity>().Where(predicate).FirstOrDefaultAsync();
        }

        public async Task<bool> UpdateOrCreate(TEntity entity)
        {
            try
            {
                TEntity dbEntity = null;
                dbEntity = _dbContext.Set<TEntity>().Find(GetKey(entity));
                if (dbEntity != null)
                {
                    _dbContext.Entry(dbEntity).CurrentValues.SetValues(entity);
                    int c = await _dbContext.SaveChangesAsync();
                    return true;
                }
                else
                {
                    var e = await CreateAsync(entity);
                    return e != null;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public bool Delete(Guid entity)
        {
            var toDelete = _dbContext.Set<TEntity>().Find(entity);
            try
            {
                _dbContext.Set<TEntity>().Remove(toDelete);
                var affectedRecords = _dbContext.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
