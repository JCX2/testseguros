﻿using DataAcces.Interfaces;
using DataAcces.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcces.Repositories
{
   public class RepositoriesHub:IRepositoryHub
    {
        private InsuranceContext _insuranceContext;

        public RepositoriesHub(IInsuranceContextProvider insuranceContextProvider )
        {
            _insuranceContext = insuranceContextProvider.GetDbCOnext();
            if (_insuranceContext != null)
            {
                CustomerRepository = new Repository<Customer>(_insuranceContext);
                CustomerInsuranceRepository = new Repository<CustomerInsurance>(_insuranceContext);
                InsuranceRepository = new Repository<Insurance>(_insuranceContext);
                UserRepository = new Repository<User>(_insuranceContext);
                InsurancesTypeRepository = new Repository<InsurancesType>(_insuranceContext);
            }
        }

        public IRepository<Customer> CustomerRepository { get; set ; }
        public IRepository<CustomerInsurance> CustomerInsuranceRepository { get ; set ; }
        public IRepository<Insurance> InsuranceRepository { get ; set ; }
        public IRepository<InsurancesType> InsurancesTypeRepository { get ; set ; }
        public IRepository<User> UserRepository { get ; set ; }
    }
}
