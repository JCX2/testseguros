﻿using DataAcces.Interfaces;
using DataAcces.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcces.Repositories
{
    public class UserRepository : IUser
    {
        private IRepositoryHub _repositoryHub;
        private IEncryptHash _encryptHash;

        public UserRepository(IRepositoryHub repositoryHub, IEncryptHash encryptHash)
        {
            _repositoryHub = repositoryHub;
            _encryptHash = encryptHash;
        }

        public async Task<User> GetUserProfile(string username)
        {
            User user = await _repositoryHub.UserRepository.GetFirstRowAsync(u => u.name.Equals(username));
            return user;
        }

        public async Task<bool> ValidateUserRequest(string username, string password)
        {
            string passHash = _encryptHash.EncriptarTexto(password);
            User user = await _repositoryHub.UserRepository.GetFirstRowAsync(u => u.name.Equals(username) && u.password.Equals(passHash));

            return user != null;
        }
    }
}
