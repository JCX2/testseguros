﻿using DataAcces.Interfaces;
using DataAcces.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcces.Repositories
{
    public class InsuranceContextProvider
        : IInsuranceContextProvider
    {
        public InsuranceContext GetDbCOnext()
        {
            return new InsuranceContext();
        }
    }
}
