using DataAcces.Interfaces;
using DataAcces.Models;
using DataAcces.Repositories;
using DataAcces.Services;
using System.Web.Http;
using Unity;
using Unity.Injection;
using Unity.WebApi;

namespace WebApiTest
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<IEncryptHash, EncryptHash>();
            container.RegisterType<IInsuranceContextProvider, InsuranceContextProvider>();
            container.RegisterType<IUser, UserRepository>();
            container.RegisterType<IRepositoryHub, RepositoriesHub>();
            container.RegisterType<IAccesTokenService, AccesTokenService>(new InjectionConstructor("QWPZguXLMj5NFk85dWuDBDYwCkVtXGB67LuTeppB"));
         
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}