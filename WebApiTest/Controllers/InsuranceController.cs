﻿using DataAcces.Interfaces;
using DataAcces.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace WebApiTest.Controllers
{
    public class InsuranceController : ApiController
    {
        private IRepositoryHub _repository;
        public InsuranceController(IRepositoryHub repository)
        {
            _repository = repository;
        }

        //[Authorize]
        [Route("api/insurance/allInsurances")]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public async Task<IHttpActionResult> Get()
        {
            //var d = await _user.UserRepository.GetAllAsync();
            //return new string[] { d.FirstOrDefault().name };
            var value = await _repository.InsuranceRepository.GetAllAsync();
            return Json(value);
        }

        [Route("api/insurance/allInsurancesTypes")]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public async Task<IHttpActionResult> GetTypes()
        {
            var value = await _repository.InsurancesTypeRepository.GetAllAsync();
            return Json(value);
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public async Task<IHttpActionResult> Post([FromBody]Insurance insurance)
        {

            var value = await _repository.InsuranceRepository.UpdateOrCreate(insurance);
            if (value)
                return Json(new { msj = "Insurance created", code = 0 });
            else
                return Json(new { msj = "Error", code = 1 });

        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }


    }
}
