﻿using DataAcces.Interfaces;
using DataAcces.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace WebApiTest.Controllers
{
    public class ValuesController : ApiController
    {
        //private IUser _user;
        //public ValuesController(IUser user) {
        //    _user = user;
        //}

        private IRepositoryHub _user;
        public ValuesController(IRepositoryHub user)
        {
            _user = user;
        }

        public ValuesController()
        {
        }

        // GET api/values
        //[Authorize]
        //public async System.Threading.Tasks.Task<IEnumerable<string>> Get()
        //{
        //    var d= await _user.UserRepository.GetAllAsync();
        //    return new string[] { d.FirstOrDefault().name };
        //    return new string[] { "value1", "value2" };
        //}
        [Authorize]
        public IEnumerable<string>Get()
        {
            //var d = await _user.UserRepository.GetAllAsync();
            //return new string[] { d.FirstOrDefault().name };
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
