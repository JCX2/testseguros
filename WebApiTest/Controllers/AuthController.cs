﻿using DataAcces.Interfaces;
using DataAcces.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Mvc;



namespace WebApiTest.Controllers
{
  
    public class AuthController : ApiController
    {
        //// GET: Auth
        //public ActionResult Index()
        //{
        //    return View();
        //}
        private IUser _userManager;
        private IRepositoryHub _repositoriesHub;
        private IAccesTokenService _accesTokenService;
        public AuthController(IUser usermanager , IRepositoryHub repositoryHub , IAccesTokenService accesTokenService)
        {
            _userManager = usermanager;
            _repositoriesHub = repositoryHub;
            _accesTokenService = accesTokenService;
        }

        //[Route("api/auth/user/token")]
        [EnableCors(origins: "*", headers: "*", methods: "*")]
        public async Task<IHttpActionResult> UserToken([FromBody] User user)
        {
            bool isValid = await _userManager.ValidateUserRequest(user.name, user.password);
            if (isValid)
            {
                User userProfile = await _userManager.GetUserProfile(user.name);
                string token = await _accesTokenService.GenerateUserTokenAsync(userProfile);

                return  Json(new
                {
                    ResponseCode = 0,
                    Message = "Succes",
                    Exp = DateTime.Now.AddDays(30).Ticks,
                    userProfile.name,
                    userProfile.UserId,
                    Token = token
                });
            }
            else
            {
                return Json(new
                {
                    ResponseCode = 1,
                    Message ="Invalid username or password",
                    Exp = string.Empty,
                    Token = string.Empty
                });
            }
        }

        //[System.Web.Mvc.Authorize]
        //public IHttpActionResult Validate()
        //{
        //    return Json(new { Text = "Authenticaded !!" });
        //}
    }
}